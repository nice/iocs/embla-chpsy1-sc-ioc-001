#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(embla-chpsy1-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./embla-chpsy1-sc-ioc-001.iocsh","IPADDR=172.30.235.21,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"EMBLA-ChpSy1:")
epicsEnvSet(R,"Chop-BWC-101:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R)")
iocInit()
#EOF
